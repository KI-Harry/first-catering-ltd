from django.db import models
from Register.models import EmployeeCards, KioskUser
from Topup.models import TopupCredit
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings

class Account(models.Model):
    card_ID = models.ForeignKey(EmployeeCards, on_delete=models.CASCADE, default = 0)
    Employee_ID = models.ForeignKey(KioskUser, on_delete=models.CASCADE, default = 0)
    credit = models.ForeignKey(TopupCredit, on_delete=models.CASCADE, default = 0)

    def get_account(self):
        Account = KioskUser.objects.filter(card_ID=data['card_ID'])

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, Created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)