from django.db import models
from django.contrib.auth.models import User, BaseUserManager, AbstractBaseUser, PermissionsMixin
import re

class KioskUserManager(BaseUserManager):
    def create_user(self, email, mobile_number, first_name, last_name, password=None):
        user = User
        if not email:
            raise ValueError("User must have an email")
        
        if not password:
            raise ValueError("User must have a password")
        
        if re.search('[a-zA-Z]', password):
            raise ValueError('The pin can only be numerics')

        user = self.model(
            email=self.normalize_email(email)
        )
        user.set_password(password)
        user.mobile_number = mobile_number
        user.first_name = first_name
        user.last_name = last_name
        user.registered = True
        user.save(using=self._db)
        return user

    def create_superuser(self, email, mobile_number, first_name, last_name, password=None):
        user = User
        if not email:
            raise ValueError("User must have an email")
        
        if not password:
            raise ValueError("User must have a password")
        
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email,
            mobile_number,
            first_name,
            last_name,
            password=password
        )
        user.is_admin = True
        user.is_staff = True
        user.save(using=self._db)
        return user

class EmployeeCards(models.Model):
    card_ID = models.CharField(max_length = 16, unique = True)

class KioskUser(AbstractBaseUser,  PermissionsMixin):
    email = models.EmailField(null= True, blank = True, unique=True)
    card_ID = models.ForeignKey(EmployeeCards, on_delete=models.CASCADE, null=True)
    employee_ID = models.CharField(max_length = 30, null = True, blank = True)
    first_name = models.CharField(max_length = 30, null = True, blank = True)
    last_name = models.CharField(max_length = 30, null = True, blank = True)
    mobile_number = models.IntegerField(null = True) #11 digits is the typical number of digits for a phone number, 14 if you're calling from outside the region
    password = models.CharField(max_length = 4) 
    registered = models.BooleanField(default = False)
    is_admin = models.BooleanField(default = False)
    is_staff = models.BooleanField(default = False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['mobile_number', 'first_name', 'last_name']
    objects = KioskUserManager()

    def __str__(self):
        return self.email

    def to_dict(self):
        #this is the basic information non the account 
        #that will display to the user
        return{
            "Name": self.name,
            "Email": self.email,
            "Mobile Number": self.mobile_number
        }

    def get_fullname(self):
        return self.first_name + " " + self.last_name