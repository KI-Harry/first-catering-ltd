from django.apps import AppConfig


class TopupConfig(AppConfig):
    name = 'Topup'
