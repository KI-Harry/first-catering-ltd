from django.db import models
from Register.models import EmployeeCards, KioskUser
from django.db.models import Sum

class TopupCredit(models.Model):
    Employee_ID = models.ForeignKey(KioskUser, on_delete=models.CASCADE, default = 0)
    credit = models.DecimalField(max_digits=100, decimal_places=2)

    def get_credit(self):
        return '''{0} Your account currently holds {1}'''(
            KioskUser.get_fullname,
            self.credit
        )

    def add_credit(self):
        add_credit_to_user = self.credit + '20'


class FoodOrder(models.Model):
    Employee_ID = models.ForeignKey(KioskUser, on_delete=models.CASCADE, default = 0)
    order_number = models.IntegerField(unique=True)

class Food(models.Model):
    PRICE = (
    (0, 'Unselected'),
    (3.40, 'Sandwhiches'),
    (4.00, 'Baguettes'),
    (8.00, 'Cooked meals'),
    (1.25, 'bottled drinks'),
    (2.00, 'hot drinks'),
    (0.55, 'Crisps/snacks')
)
    food_type = models.CharField(max_length = 10)
    food_name = models.CharField(max_length = 30)
    price = models.DecimalField(choices = PRICE, decimal_places=2)
    order_ID = models.ForeignKey(FoodOrder, on_delete=models.CASCADE, default = 0, null=True)
    number_available = models.IntegerField()

def __str__(self):
    return '''We have {0} {1} available for purchase today it costs {2}'''(
        self.number_available,
        self.food_name,
        self.price
    )

def check_food_available(self):
    if self.number_available == 0: 
        return '''This food is out of stock'''

@property
def calculate_total_cost(self):
    food_price_list = [
        Food.price 
        for Food
        in self.food_set.all()
    ]

    return sum(food_price_list)