from django.test import TestCase
from Register.models import KioskUser, EmployeeCards
from Topup.models import TopupCredit, FoodOrder, Food
class TestCredit(TestCase):
    def setUp(self):
        #setup user and card ID
        my_card_one = EmployeeCards.objects.create(card_ID = 'HARH0000001241HH')
        my_user = KioskUser.objects.create(email = 'Harry@keyintelligence.uk', card_ID = my_card_one, employee_ID = 'HH01KeyInt',
        first_name = 'Harry', last_name = 'Holdstock', mobile_number = '07704773654', password = '1701', registered = True, is_admin = True)
        
        #setup the credit the user has
        user_credit = TopupCredit(card_ID = my_card_one, credit = '50')
        
        #setup the order ready for the user to make a purchase
        my_order = FoodOrder(Employee_ID = my_user, order_number = '1')

        food = (food_type = 'Sandwich', food_name = 'Egg and Cress, Sandwhich', price = '3.4', order = my_order, number_available = 15)
        food_two = (food_type = 'Baguette', food_name = 'Ham and Cheese with mustard, Baguette', price = '4.00', order = my_order, number_available = 1)

    def test_price_of_food(self):
        Food.objects.get(price)
