from rest_framework import serializers
from Register.models import EmployeeCards, KioskUser
from Account.models import Account
from Topup.models import TopupCredit, FoodOrder, Food

class KioskUserSerializer(serializers.HyperlinkedModelSerializer):
    
    class meta:
        model = KioskUser
        fields = ('url', 'email', 'card_ID', 'employee_ID', 'first_name', 'last_name', 'mobile_number', 'password', 'registered')

class CardSerializer(serializers.HyperlinkedModelSerializer):

    class meta: 
        model = EmployeeCards
        fields = ('url', 'card_ID')

class TopupSerializer(serializers.HyperlinkedModelSerializer):

    class meta:
        model = TopupCredit
        fields = ('url', 'card_ID', 'credit')

class OrderSerializer(serializers.HyperlinkedModelSerializer):

    class meta: 
        model = FoodOrder
        fields = ('url', 'card_id', 'order_number')

class FoodSerializer(serializers.HyperlinkedModelSerializer):

    class meta:
        model = Food
        fields = ('url', 'food_type', 'food_name', 'price', 'order_id', 'number_available')