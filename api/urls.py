"""Cinema URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.urls import path
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.schemas import get_schema_view
from rest_framework.authtoken import views as authtokenview
from api import views

router = routers.DefaultRouter()
router.register(r'account', views.AccountViewSet)
router.register(r'user', views.KioskUserViewSet)
router.register(r'employeecards', views.EmployeeCardViewSet)
router.register(r'topupcredit', views.TopupViewSet)
router.register(r'orders', views.OrderViewSet)
router.register(r'food', views.FoodViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^docs/$', views.schema_view)
]