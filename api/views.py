from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework import viewsets, generics, status
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from django.conf.urls import url
from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from django_filters import rest_framework as filters
import django_filters.rest_framework
from django.utils.functional import cached_property
from api.serializers import KioskUserSerializer, CardSerializer, TopupSerializer, OrderSerializer, FoodSerializer
from Register.models import KioskUser, EmployeeCards
from Topup.models import TopupCredit, FoodOrder, Food

class EmployeeCardViewSet(viewsets.ModelViewSet):

    queryset = EmployeeCards.objects.all()
    serializer_class = CardSerializer
    filter_backends = (filters.DjangoFilterBackend)
    filterset_fields = ('card_ID')

class TopupViewSet(viewsets.ModelViewSet):

    queryset = TopupCredit.objects.all()
    serializer_class = TopupSerializer
    filter_backends = (filters.DjangoFilterBackend)
    filterset_fields = ('card_ID', 'credit')

class OrderViewSet(viewsets.ModelViewSet):

    queryset = FoodOrder.objects.all()
    serializer_class = OrderSerializer
    filter_backends = (filters.DjangoFilterBackend)
    filterset_fields = ('user_ID', 'order_number')

class FoodViewSet(viewsets.ModelViewSet):

    queryset = FoodOrder.objects.all()
    serializer_class = FoodSerializer
    filter_backends = (filters.DjangoFilterBackend)
    filterset_fields = ('food_type', 'food_name', 'price', 'order_ID', 'number_available')

class AccountViewSet(viewsets.ModelViewSet):

    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    filter_backends = (filters.DjangoFilterBackend)
    filterset_fields = ('card_ID', 'user_ID', 'credit')

class KioskUserViewSet(viewsets.ModelViewSet):

    queryset = KioskUser.objects.all()
    serializer_class = KioskUserSerializer
    filter_backends = (filters.DjangoFilterBackend)
    filterset_fields =  ('email', 'card_ID', 'employee_ID', 'first_name', 'last_name', 'mobile_number', 'registered')
    